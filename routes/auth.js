import express from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
const router = express.Router();

//module
import User from '../models/User';

//create user
router.post('/register', async (req, res) => {
    let { username, email, password, created_on, last_login } = req.body;
    console.log(req.body);

    // Check email
    const emailExist = await User.findAll({
        where: {
            email: email
        }
    });
    if (emailExist.length > 0) {
        res.status(400).json({ result: 'Failed', message: `Email already exist` });
    }

    //Hash passwords
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);

    try {
        let newUser = await User.create({
            username,
            email,
            password: hashPassword,
            created_on,
            last_login
        }, {
            fields: ["username", "email", "password", "created_on", "last_login"]
        }
        );

        if (newUser) {
            res.status(200).json({
                result: 'Success',
                data: newUser,
                message: `Insert a new User Success`
            });
        } else {
            res.status(200).json({ result: 'Failed', message: `Insert a new User failed` });
        }
    } catch (error) {
        res.status(200).json({ result: 'Failed', message: `Insert a new User failed. ERROR: ` + error });
    }

});


router.post('/login', async (req, res) => {
    let { email, password } = req.body;

    // Checking if the email exist
    const user = await User.findAll({
        where: {
            email: email
        }
    });

    if (user.length <= 0) {
        res.status(400).json({ result: 'Failed', message: `Email is not found` });
    }

    // Password is correct 
    const validPassword = await bcrypt.compare(password, user[0].password);
    console.log(validPassword);

    if (!validPassword) {
        res.status(400).json({ result: 'Failed', message: `Invalid password` });
    }

    // Create and assign a token
    const token = jwt.sign({ _id: user[0].user_id }, process.env.TOKEN_SECRET, {
        expiresIn: 86400 // expires in 24 hours
    });

    // res.header('Authorization', token).json({
    //     result: 'Success',
    //     token: token,
    //     userId: user[0].id,
    //     username: user[0].username,
    //     message: `Logged in`
    // });

    res.cookie('access_token', token, {
        maxAge: 365 * 24 * 60 * 60 * 100, // thời gian sống 
        httpOnly: true, // chỉ có http mới đọc được token
        secure: true //ssl nếu có, nếu chạy localhost thì comment nó lại
    })
    res.status(200).json({
        result: 'Success',
        message: `Logged in`,
        token: token,
        username: user[0].username,
        userId: user[0].id
    });

})

export default router;
