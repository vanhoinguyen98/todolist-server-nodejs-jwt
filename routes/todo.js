import express from 'express';
import passport from 'passport';
const router = express.Router();

//module
import Todo from '../models/Todo';

//Add todo
router.post('/:user_id/', async (req, res) => {
    const { user_id } = req.params;
    let { title, completed, created_on } = req.body;
    console.log("add todos");

    console.log(req.body);

    console.log(created_on);
    console.log(title);


    try {
        let newTodo = await Todo.create({
            user_id,
            title,
            completed,
            created_on
        }, {
            fields: ["user_id", "title", "completed", "created_on"]
        }
        );

        if (newTodo) {
            res.json({
                result: 'Success',
                data: newTodo,
                message: `Insert a new Todo Success`
            });
        } else {
            res.json({
                result: 'Failed',
                data: {},
                message: `Insert a new Todo failed`
            });
        }
    } catch (error) {
        res.json({
            result: 'Failed',
            data: {},
            message: `Insert a new Todo failed. ERROR: ` + error
        });
    }

});


// Update Todo
router.put('/:user_id/:todo_id', async (req, res) => {
    const { user_id, todo_id } = req.params;
    const { title, completed } = req.body;
    try {
        let updateTodo = await Todo.findAll({
            attributes: ['id', 'user_id', 'title', 'completed', 'created_on'],
            where: {
                id: todo_id,
                user_id: user_id
            }
        });

        if (updateTodo.length > 0) {
            updateTodo.forEach(async (todo) => {
                await todo.update({
                    title: title ? title : todo.title,
                    completed: completed !== todo.completed ? completed : todo.completed,
                });
            });
            res.json({
                result: "success",
                data: updateTodo,
                message: "Update a todo success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Can not find to do update"
            });
        }
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Can not find to do update. ERROR: " + error
        });
    }

});

// Delete Todo
router.delete('/:user_id/:todo_id', async (req, res) => {
    const { user_id, todo_id } = req.params;
    try {
        let numberOfDeleteRows = await Todo.destroy({
            where: {
                id: todo_id,
                user_id: user_id
            }
        });
        if (numberOfDeleteRows < 1) {
            res.json({
                result: "Failed",
                data: {},
                message: "Todo has not in list" + error
            });
        }
        else {
            res.json({
                result: "success",
                count: numberOfDeleteRows,
                message: "Delete a todo success"
            });
        }
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Can not to do delete. ERROR: " + error
        });
    }
});

// Delete All todo completed 
router.post('/all/:user_id/', async (req, res) => {
    const { user_id } = req.params;
    const { arrTodoCompleted } = req.body;
    try {
        arrTodoCompleted.forEach(async (todo_id) => {
            await Todo.destroy({
                where: {
                    id: todo_id,
                    user_id: user_id
                }
            });
        });

        res.json({
            result: "success",
            message: "Delete a todo success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            message: "Can not to do delete. ERROR: " + error
        });
    }
});

// Query all data from db
router.get('/', async (req, res) => {
    try {
        let todos = await Todo.findAll({
            attributes: ['user_id', 'id', 'title', 'completed', 'created_on'],
        });
        res.json({
            result: "success",
            data: todos,
            length: todos.length,
            message: "Query list of todos success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            length: todos.length,
            message: "Query list of todos failed. ERROR: " + error
        });
    }
});

// Get Todo by todo id
router.get('/:id', async (req, res) => {
    const { id } = req.params;

    try {
        let todos = await Todo.findAll({
            attributes: ['user_id', 'id', 'title', 'completed', 'created_on'],
            where: {
                id: id
            }
        });

        if (todos.length > 0) {
            res.json({
                result: "success",
                data: todos[0],
                message: "Query id success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Querry id todo failed. ERROR: " + error
            });
        }

    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Querry id todo failed. ERROR: " + error
        });
    }
});

// Get Todo by user id
router.get('/userid/:user_id', async (req, res) => {
    const { user_id } = req.params;

    try {
        let todos = await Todo.findAll({
            attributes: ['user_id', 'id', 'title', 'completed', 'created_on'],
            where: {
                user_id: user_id
            }
        });

        if (todos.length > 0) {
            res.json({
                result: "success",
                data: todos,
                message: "Query id success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Querry id todo failed. ERROR: " + error
            });
        }

    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Querry id todo failed. ERROR: " + error
        });
    }
});

export default router;
