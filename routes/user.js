import express from 'express';
import passport from 'passport';
import bcrypt from 'bcryptjs';
const router = express.Router();

//module
import User from '../models/User';
import Todo from '../models/Todo';

// Update User
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { username, email, password } = req.body;
    try {
        let updateUser = await User.findAll({
            attributes: ['id', 'username', 'password', 'email', 'created_on', 'last_login'],
            where: {
                id: id
            }
        });

        if (updateUser.length > 0) {
            updateUser.forEach(async (user) => {
                await user.update({
                    username: username ? username : user.username,
                    email: email ? email : user.email,
                    password: password ? password : user.password,
                });
            });
            res.json({
                result: "success",
                data: updateUser,
                message: "Update a user success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Can not find user  update"
            });
        }
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Can not find user update. ERROR: " + error
        });
    }

});

// Delete a user
router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        await Todo.destroy({
            where: {
                user_id: id
            }
        });
        let numberOfDeleteRows = await User.destroy({
            where: {
                id: id
            }
        });
        if (numberOfDeleteRows < 1) {
            res.json({
                result: "Failed",
                data: {},
                message: "User has not in list" + error
            });
        }
        else {
            res.json({
                result: "success",
                count: numberOfDeleteRows,
                message: "Delete a user success"
            });
        }
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Can not user delete. ERROR: " + error
        });
    }
});

// Query all data from db
router.get('/', async (req, res) => {
    try {
        let users = await User.findAll({
            attributes: ['id', 'username', 'password', 'email', 'created_on', 'last_login'],
        });
        res.json({
            result: "success",
            data: users,
            length: users.length,
            message: "Query list of user success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            length: users.length,
            message: "Query list of user failed. ERROR: " + error
        });
    }
});

// Query a user from user id 

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        let user = await User.findAll({
            attributes: ['id', 'username', 'password', 'email', 'created_on', 'last_login'],
            where: {
                id: id
            }
        });

        if (user.length > 0) {
            res.json({
                result: "success",
                data: user[0],
                message: "Query id success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Querry id user failed. ERROR: " + error
            });
        }

    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Querry id user failed. ERROR: " + error
        });
    }
});

export default router;
