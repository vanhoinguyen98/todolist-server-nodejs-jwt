import jwt from 'jsonwebtoken';

module.exports = function (req, res, next) {
    // const token = req.header('Authorization');
    const token = req.cookies.access_token;
    console.log("Cookies");
    console.log(req.cookies.access_token);
    console.log(req.cookies);
    if (!token) return res.status(401).json({ message: 'Access Denied' })
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        console.log("verified");
        console.log(verified);
        req.user = verified;
        next();
    }
    catch (err) {
        return res.status(400).json({ message: 'Invalid Token' })
    }
}