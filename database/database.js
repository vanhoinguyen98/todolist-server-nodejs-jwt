require('dotenv').config();
import Sequelize from 'sequelize';
export const sequelize = new Sequelize(
    process.env.DB_NAME, //dbname
    process.env.DB_USERNAME, //username
    process.env.DB_PASSWORD, // password
    {
        dialect: 'postgres', // or mysql ,...
        host: process.env.HOST,
        operatorAliases: false,
        pool: {
            max: 5,
            min: 0,
            require: 30000,
            idle: 10000
        }
    }
);

export const Op = Sequelize.Op;