import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

const Todo = sequelize.define('todos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING
    },
    completed: {
        type: Sequelize.BOOLEAN
    },
    created_on: {
        type: Sequelize.DATE
    }
}, {
    // dont add the timestamp attributes ( updatedAt, createdAt)
    timestamps: false,
}
);

export default Todo;