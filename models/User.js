import Sequelize from 'sequelize';
import { sequelize } from '../database/database';
import Todo from './Todo';

const User = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    username: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    created_on: {
        type: Sequelize.DATE
    },
    last_login: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
}
);

User.hasMany(Todo, { foreignKey: 'id', sourceKey: 'id' });
Todo.belongsTo(User, { foreignKey: 'user_id', targetKey: 'id' });

export default User;