require('dotenv').config();
import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

import cors from 'cors';

import todoRouter from './routes/todo';
import userRouter from './routes/user';
import indexRouter from './routes/index';
import authRouter from './routes/auth';
import verify from './middlewares/VerifyToken';

const app = express();

// view engine setup
app.engine('ejs', require('express-ejs-extend'));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(cors())
// app.use(cors({
//   origin: 'https://deloy-demo-tvn-group.firebaseapp.com/', //Chan tat ca cac domain khac ngoai domain nay
//   credentials: true //Để bật cookie HTTP qua CORS
// }))

const whitelist = ['https://vuejs-todolist.firebaseapp.com', 'http://localhost:8080', 'https://vuejs-todolist.web.app']

app.use(cors({
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  credentials: true //Để bật cookie HTTP qua CORS
}))

// app.all("*", function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Authorization, X-Requested-With");
//   res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS"); /* Những phương thức mà server sẽ hỗ trợ */
//   res.header("Access-Control-Allow-Credentials", "false"); /* Bật lên khi bạn sử dụng Cookie */
//   res.header("Access-Control-Max-Age", 300000); /* Thời gian hiệu lực của yêu cầu tiền trạm */
//   if (req.method === "OPTIONS") return res.send(200); /* Cho phép các request với phương thức OPTIONS phản hồi lại nhanh */
//   else next();
// });

// API EXAMPLE
app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/todos', verify, todoRouter);
app.use('/users', verify, userRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
